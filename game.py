def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over():
    print_board(board)
    print("GAME OVER")
    print(current_player, "has won")
    exit()

def row_is_winner(row):
    product = row * 3
    if board[0 + product] == board[1 + product] and board[0 + product] == board[2 + product]:
        return True
    return False

def column_is_winner(column):
    product = column
    if board[0 + product] == board[3 + product] and board[0 + product] == board[6 + product]:
        return True
    return False

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if row_is_winner(0):
        game_over()
    elif row_is_winner(1):
        game_over()
    elif row_is_winner(2):
        game_over()
    elif column_is_winner(0):
        game_over()
    elif column_is_winner(1):
        game_over()
    elif column_is_winner(2):
        game_over()
    elif board[0] == board[4] and board[4] == board[8]:
        game_over()
    elif board[2] == board[4] and board[4] == board[6]:
        game_over()

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
